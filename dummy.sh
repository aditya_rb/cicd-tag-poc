#! /bin/bash

echo "starting files."
echo "${1}"
# https://gist.github.com/DarrenN/8c6a5b969481725a4413#gistcomment-1678696
PACKAGE_VERSION=$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

str="${1}"
IFS=',' # , is set as delimiter
read -ra allLabel <<< "$str" # str is read into an array as tokens separated by IFS

IFS='.' # . is set as delimiter
read -ra versionArray <<< "$PACKAGE_VERSION" # PACKAGE_VERSION is read into an array as tokens separated by IFS

check_log(){
    for i in "${allLabel[@]}"; do # access each element of array
        if [ $i == $1 ];
        then 
            return 0
        fi
    done
    return 1
}

updateVersion(){
    versionArray[$1]=$((versionArray[$1]+1))
    var=$( IFS=.; echo "${versionArray[*]}" )
    echo "${var}"
    json -I -f package.json -e "this.version='$var'"
    cat package.json
    git tag "$var"
    git push --tags
}

if  check_log "improvement";
    then
        updateVersion 0
elif check_log "release"
    then 
        echo "version updating"
        updateVersion 1
elif check_log "bug-fix"
    then
        updateVersion 2
fi