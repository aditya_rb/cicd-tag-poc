#!/usr/bin/env node

const https = require('https');
const exec = require('child_process').exec;
(async()=>{
    let vType;
    const args = process.argv.slice(2);
    const url = `https://gitlab.com/api/v4/projects/${args[0]}/merge_requests/${args[1]}`;
        try{
        const response = new Promise((resolve, reject)=>{
        https.get(url, (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
                data += chunk;
            });
            resp.on('end', () => {
                resolve(JSON.parse(data));
                });
            }).on("error", (err) => {
                reject('Error occured')
            });
        })

        const {labels} = await response
        if(labels.includes('improvement')){
            vType = 'major'
        }else if(labels.includes('release')){
            vType = 'minor'
        }else if(labels.includes('bug-fix')){
            vType = 'patch'
        }
        exec(`npm version ${vType};git push --tags`, (err, stdout, stderr)=>{console.log(err, stdout, stderr)})
        }catch(error){console.log(err)}
})()
