const exec = require("child_process").exec;
const bodyParser = require("body-parser");
// const LogsService = require("./service/logService.js");
const express = require("express");
const app = express();
const port = 5000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const getDescription = (labels, tagName) => {
  const tagData = labels.filter(el => el.title === tagName);
  return tagData[0].description;
};
app.get("/", (req, res) => {
  const testString = `hello, I am running.`;
  console.log(testString);
  res.send(testString);
});
app.post("/updateversion", (req, res) => {
  let vType, description;
  var errObj = {
    Agent: "BUSHIRE",
    LogType: "RVM",
    repoName: "",
    Error: null,
    IsScuccess: false,
    source: "Label creation."
  };
  console.log(req.body)
  const {
    labels,
    project: { name, git_ssh_url },
    object_attributes,
    event_type
  } = req.body;
  const { target_branch, state } = object_attributes;
  console.log(`Starting process for repo: ${name}`);
  if (
    target_branch === "master" &&
    // state === "merged" &&
    event_type === "merge_request"
  ) {
    try {
      if (labels.length === 0) {
        throw "No tags available.";
      }
      const labelArray = labels.map(el => el.title);
      if (labelArray.includes("feature")) {
        vType = "major";
        description = getDescription(labels, "feature");
      } else if (labelArray.includes("enhancement")) {
        vType = "minor";
        description = getDescription(labels, "enhancement");
      } else if (labelArray.includes("bug-fix")) {
        vType = "patch";
        description = getDescription(labels, "bug-fix");
      } else {
        throw "Invalid tag.";
      }
      exec(
        `cd Desktop; rm -rf ${name}; git clone ${git_ssh_url}; cd ${name};git checkout master; npm version ${vType}; git push --tags; git add .; git commit -m "tag-Updated";  git push origin master; cd ..; rm -rf ${name}`,
        (err, stdout, stderr) => {
          console.log(stdout, stderr);
          if (err) {
            errObj.Error = err;
            errObj.repoName = name;
            errObj.source = "Bash script";
            // LogsService.logInKibana(errObj);
          }
        }
      );
    } catch (error) {
      errObj.Error = error;
      errObj.repoName = name;
      errObj.merge_state = state;
      // LogsService.logInKibana(errObj);
    }
  }
  res.end();
});

app.listen(port, () =>
  console.log(`Hooks server app is listening on port ${port}!`)
);
